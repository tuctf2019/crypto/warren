#!/usr/bin/env python3

import socket, sys, string

from select import select

from _thread import *

SERVER_MODE = False

HOST = ''
PORT = 8888

TIMEOUT = 90

FLAG = 'TUCTF{th4nks_f0r_d1n1ng_4641n_4t_th3_W4rr3n_buff3t}'

INTRO = """
Welcome to the Warren Buffet!

You must solve all of the ciphers
to receive the flag.

I'd recommend filling your plate
and solving all at once.
(The timeout is 90 seconds)

"""

OUTRO = """
Thanks for dining at the Warren buffet!

Here's your flag:

"""

AFFINE_PLAINTEXT = 'AFFINE IS A BAD CIPHER'
BACONIAN_PLAINTEXT = 'BACONIAN IS ENJOYABLE'
CAESAR_PLAINTEXT = 'CAESAR IS AN EASY CIPHER'
ATBASH_PLAINTEXT = 'ATBASH IS A FUN CIPHER'
VIGENERE_PLAINTEXT = 'VIGENERE IS A HARD CIPHER'

VIGENERE_KEY = 'TUCTF'

BACONIAN_KEYMAP = {
    'A' : 'aaaaa',
    'B' : 'aaaab',
    'C' : 'aaaba',
    'D' : 'aaabb',
    'E' : 'aabaa',
    'F' : 'aabab',
    'G' : 'aabba',
    'H' : 'aabbb',
    'I' : 'abaaa',
    'J' : 'abaaa',
    'K' : 'abaab',
    'L' : 'ababa',
    'M' : 'ababb',
    'N' : 'abbaa',
    'O' : 'abbab',
    'P' : 'abbba',
    'Q' : 'abbbb',
    'R' : 'baaaa',
    'S' : 'baaab',
    'T' : 'baaba',
    'U' : 'baabb',
    'V' : 'baabb',
    'W' : 'babaa',
    'X' : 'babab',
    'Y' : 'babba',
    'Z' : 'babbb',
    ' ' : ' '
}

class Ciphers:

    @staticmethod
    def Affine(s, a, b):
        return ''.join([chr(((a*(ord(t) - ord('A')) + b) % 26)
                  + ord('A')) if t != ' ' else ' ' for t in s])

    @staticmethod
    def Baconian(s):
        return ''.join([BACONIAN_KEYMAP[c] for c in s])

    @staticmethod
    def Caesar(s, k):
        o = ''
        k = k % 26
        for c in s:
            if c in string.ascii_uppercase:
                c = chr(((ord(c) - ord('A')) + k) % 26 + ord('A'))
            o += c
        return o

    @staticmethod
    def Atbash(s):
        keymap = dict(zip(list(string.ascii_uppercase), list(string.ascii_uppercase[::-1])))
        keymap[' '] = ' '
        return ''.join([keymap[c] for c in s])

    @staticmethod
    def Vigenere(s, k):
        k = [(ord(c) - ord('A')) for c in k]
        o = ''
        ki = 0
        for i in range(len(s)):
            if s[i] == ' ':
                o += ' '
            else:
                o += chr(((ord(s[i]) - ord('A')) + k[ki % len(k)]) % 26 + ord('A'))
                ki += 1
        return o

def handle_affine(conn):
    CIPHER = Ciphers.Affine(AFFINE_PLAINTEXT, 17, 20)
    if conn is None:
        sys.stdout.write(f'\nHave you heard of this one? Because I just read about it.\nHere\'s your cipher:\n\n{CIPHER}\n\nGive the plaintext: ')
        sys.stdout.flush()

        rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()

        data = data.rstrip('\n').upper()
        if data == AFFINE_PLAINTEXT:
            sys.stdout.write('Correct!\n\n')
            sys.stdout.flush()
            SOLVED['Affine'] = True
        else:
            sys.stdout.write('Incorrect\n\n')
            sys.stdout.flush()
    else:
        conn.send((f'\nHave you heard of this one? Because I just read about it.\nHere\'s your cipher:\n\n{CIPHER}\n\nGive the plaintext: ').encode('utf-8'))

        data = conn.recv(1024)
        data = data.decode('utf-8').rstrip('\n').upper()

        if data == AFFINE_PLAINTEXT:
            conn.send('Correct!\n\n'.encode('utf-8'))
            SOLVED['Affine'] = True
        else:
            conn.send('Incorrect\n\n'.encode('utf-8'))

def handle_baconian(conn):
    CIPHER = Ciphers.Baconian(BACONIAN_PLAINTEXT)
    if conn is None:
        sys.stdout.write(f'\nHave some bacon!\nHere\'s your cipher:\n\n{CIPHER}\n\nGive the plaintext: ')
        sys.stdout.flush()

        rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()

        data = data.rstrip('\n').upper()
        if data == BACONIAN_PLAINTEXT:
            sys.stdout.write('Correct!\n\n')
            sys.stdout.flush()
            SOLVED['Baconian'] = True
        else:
            sys.stdout.write('Incorrect\n\n')
            sys.stdout.flush()
    else:
        conn.send((f'\nHave some bacon!\nHere\'s your cipher:\n\n{CIPHER}\n\nGive the plaintext: ').encode('utf-8'))

        data = conn.recv(1024)
        data = data.decode('utf-8').rstrip('\n').upper()

        if data == BACONIAN_PLAINTEXT:
            conn.send('Correct!\n\n'.encode('utf-8'))
            SOLVED['Baconian'] = True
        else:
            conn.send('Incorrect\n\n'.encode('utf-8'))

def handle_caesar(conn):
    CIPHER = Ciphers.Caesar(CAESAR_PLAINTEXT, 20)
    if conn is None:
        sys.stdout.write(f'\nReady for a classic?\nHere\'s your cipher:\n\n{CIPHER}\n\nGive the plaintext: ')
        sys.stdout.flush()

        rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()

        data = data.rstrip('\n').upper()
        if data == CAESAR_PLAINTEXT:
            sys.stdout.write('Correct!\n\n')
            sys.stdout.flush()
            SOLVED['Caesar'] = True
        else:
            sys.stdout.write('Incorrect\n\n')
            sys.stdout.flush()
    else:
        conn.send((f'\nReady for a classic?\nHere\'s your cipher:\n\n{CIPHER}\n\nGive the plaintext: ').encode('utf-8'))

        data = conn.recv(1024)
        data = data.decode('utf-8').rstrip('\n').upper()

        if data == CAESAR_PLAINTEXT:
            conn.send('Correct!\n\n'.encode('utf-8'))
            SOLVED['Caesar'] = True
        else:
            conn.send('Incorrect\n\n'.encode('utf-8'))

def handle_atbash(conn):
    CIPHER = Ciphers.Atbash(ATBASH_PLAINTEXT)
    if conn is None:
        sys.stdout.write(f'\nThis is a fun one that\'s less common!\nHere\'s your cipher:\n\n{CIPHER}\n\nGive the plaintext: ')
        sys.stdout.flush()

        rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()

        data = data.rstrip('\n').upper()
        if data == ATBASH_PLAINTEXT:
            sys.stdout.write('Correct!\n\n')
            sys.stdout.flush()
            SOLVED['Atbash'] = True
        else:
            sys.stdout.write('Incorrect\n\n')
            sys.stdout.flush()
    else:
        conn.send((f'\nThis is a fun one that\'s less common!\nHere\'s your cipher:\n\n{CIPHER}\n\nGive the plaintext: ').encode('utf-8'))

        data = conn.recv(1024)
        data = data.decode('utf-8').rstrip('\n').upper()

        if data == ATBASH_PLAINTEXT:
            conn.send('Correct!\n\n'.encode('utf-8'))
            SOLVED['Atbash'] = True
        else:
            conn.send('Incorrect\n\n'.encode('utf-8'))

def handle_vigenere(conn):
    CIPHER = Ciphers.Vigenere(VIGENERE_PLAINTEXT, VIGENERE_KEY)
    if conn is None:
        sys.stdout.write(f'\nThis is a tough one!\nHere\'s your cipher:\n\n{CIPHER}\n\nGive the plaintext: ')
        sys.stdout.flush()

        rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()

        data = data.rstrip('\n').upper()
        if data == VIGENERE_PLAINTEXT:
            sys.stdout.write('Correct!\n\n')
            sys.stdout.flush()
            SOLVED['Vigenere'] = True
        else:
            sys.stdout.write('Incorrect\n\n')
            sys.stdout.flush()
    else:
        conn.send((f'\nThis is a tough one!\nHere\'s your cipher:\n\n{CIPHER}\n\nGive the plaintext: ').encode('utf-8'))

        data = conn.recv(1024)
        data = data.decode('utf-8').rstrip('\n').upper()

        if data == VIGENERE_PLAINTEXT:
            conn.send('Correct!\n\n'.encode('utf-8'))
            SOLVED['Vigenere'] = True
        else:
            conn.send('Incorrect\n\n'.encode('utf-8'))

OPTIONS = [
    'Affine',
    'Baconian',
    'Caesar',
    'Atbash',
    'Vigenere',
]

SOLVED = dict(zip(OPTIONS, [False]*len(OPTIONS)))

FN_DICT = {
    'Affine'   : handle_affine,
    'Baconian' : handle_baconian,
    'Caesar'   : handle_caesar,
    'Atbash'   : handle_atbash,
    'Vigenere' : handle_vigenere
}

def timeoutfunc():
    sys.stdout.write('\nYou idled too long\n\n')
    sys.stdout.flush()
    sys.exit(0)

def handle_menu(conn):
    MENU = '\n'.join([f'{i+1}) {OPTIONS[i]}' + (' [solved]' if SOLVED[OPTIONS[i]] else '') for i in range(len(OPTIONS))])

    if conn is None:
        sys.stdout.write(f'\nMENU:\n{MENU}\n')
        sys.stdout.flush()

        rlist, _, _ = select([sys.stdin], [], [], TIMEOUT)
        if rlist:
            data = sys.stdin.readline()
        else:
            timeoutfunc()

        try:
            choice = int(data.rstrip('\n'))
            if choice >= 1 and choice <= len(OPTIONS):
                option = OPTIONS[choice-1]
                if SOLVED[option]:
                    sys.stdout.write('You\'ve already solved that one!\n')
                    sys.stdout.flush()
                else:
                    FN_DICT[option](conn)
            else:
                sys.stdout.write('Not a valid option\n')
                sys.stdout.flush()
        except ValueError:
            sys.stdout.write('Input needs to be an integer\n')
            sys.stdout.flush()
    else:
        conn.send((f'{MENU}\n').encode('utf-8'))
        data = conn.recv(16)
        data = data.decode('utf-8').rstrip('\n')

        try:
            choice = int(data)
            if choice >= 1 and choice <= len(OPTIONS):
                option = OPTIONS[choice-1]
                if SOLVED[option]:
                    conn.send('You\'ve already solved that one!\n'.encode('utf-8'))
                else:
                    FN_DICT[option](conn)
            else:
                conn.send('Not a valid option\n'.encode('utf-8'))
        except ValueError:
            conn.send('Input needs to be an integer\n'.encode('utf-8'))

if SERVER_MODE:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Socket created')

    try:
        s.bind((HOST, PORT))
    except socket.error as msg:
        print('Bind failed. Error code: ' + str(msg[0]) + ' Message ' + msg[1])
        sys.exit()

    print('Socket bind complete')

    s.listen(10)
    print('Socket now listening')

def clienthread(conn):
    try:
        conn.send((INTRO).encode('utf-8'))

        all_solved = False
        while not all_solved:
            all_solved = True
            for option in OPTIONS:
                if not SOLVED[option]:
                    all_solved = False
                    break
            if all_solved:
                break

            handle_menu(conn)

        conn.send((f'{OUTRO}\n{FLAG}\n\n').encode('utf-8'))

        conn.close()
    except (BrokenPipeError, ConnectionResetError):
        pass
    except socket.timeout:
        conn.send('\nYou idled too long.\n'.encode('utf-8'))

cont = True
while cont:
    if SERVER_MODE:
        try:
            conn, addr = s.accept()
            print('Connected with ' + addr[0] + ': ' + str(addr[1]))
            conn.settimeout(TIMEOUT)

            start_new_thread(clienthread, (conn,))
        except KeyboardInterrupt:
            sys.stdout.write('Shutting down\n')
            sys.stdout.flush()
            sys.exit(0)
    else:
        try:
            sys.stdout.write(INTRO)
            sys.stdout.flush()
            conn = None

            all_solved = False
            while not all_solved:
                all_solved = True
                for option in OPTIONS:
                    if not SOLVED[option]:
                        all_solved = False
                        break
                if all_solved:
                    break

                handle_menu(conn)

            sys.stdout.write(f'{OUTRO}\n{FLAG}\n\n')
            sys.stdout.flush()

            cont = False
        except KeyboardInterrupt:
            sys.stdout.write('\n')
            sys.stdout.flush()
            sys.exit(0)

if SERVER_MODE:
    s.close()
