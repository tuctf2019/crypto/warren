# Warren -- TUCTF2019

It tells you what the ciphers are, and they don't change. Use a solver of your choice.

Flag: `TUCTF{th4nks_f0r_d1n1ng_4641n_4t_th3_W4rr3n_buff3t}`
