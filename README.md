# Warren

Desc: `A true cipher buffet lies ahead.`

Flag: `TUCTF{th4nks_f0r_d1n1ng_4641n_4t_th3_W4rr3n_buff3t}`

Note: if running as a server, you need to use `socat`. Otherwise, the first solve will persist, and the chal will be solved for everybody. (See how the [docker image](https://gitlab.com/tuctf2019/images/crypto/warren) works)
